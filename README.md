# The NAStJA Framework

Neoteric Autonomous Stencil code for Jolly Algorithms

The NAStJA framework provides an easy way to enable massively parallel simulations for a wide range of multi-physics applications based on stencil algorithms. 
It supports the development of parallel strategies for high-performance computing. 
Modern C++ and the usage of template metaprogramming achieve excellent performance without losing ﬂexibility and usability.


### Purpose
The NAStJA Tutorial is an introductory and comprehensive tutorial for beginners.\
It is broken up into beginner, intermediate, and advanced sections, as well as sections covering specific topics.

For a Quick Guide please check [Quick Guide to NASTjA](./tutorial/quick_guide.md).\
For further information please check the full documentation [https://nastja.gitlab.io/nastja/docs/index.html](https://nastja.gitlab.io/nastja/docs/index.html).


### Let's get started
To get a quick peak about this tutorial, here's a quick overview.

 - [Introduction]()
 - [beginner]()
    - [Set Up]()
    - [first Steps]()
    - [visualization]()
 - [intermediate]()
 - [advanced]()
 - [data precessing]()
 - [specific topics]()
    - [variables in input file]()

It's not absolutley necessarily to follow this specific order, but it's recommended.\
If you just want to discover the possibilitys of NASTjA, click [](here) for some amazing pictures.