# Set Up
First at all you need to Set Up NASTjA and all it's dependencys.

Please pay attention, that NASTjA supports only Linux, macOS and UNIX systems.

To start it's enough to install, you can easily follow the linked guides:
 - [CMake 3.10+](https://cmake.org/install/)
 - C++14 compliant compilter:
    - [Clang 6, 7, 8, 9](https://clang.llvm.org/get_started.html)
    - [GCC 7, 8](https://gcc.gnu.org/install/)
 - [Message Passing Interface (MPI) library](https://www.open-mpi.org/faq/?category=building)

Now let's start installing NASTjA:
 1. Check out the NASTjA project:
    - Change directory to where you want the NASTjA directory placed
    - ```git clone https://gitlab.com/nastja/nastja.git```
 2. Build NASTjA
    - ```cd nastja```
    - ```mkdir build```
    - ```cd build```
    - ```cmake ..```
    - ```make``` (please note, you can also use ```make -j <NUM_PROC>```, with ```<NUM_PROC>``` is the number of processor + 1, to speed things up)

Please note: RAM > 4GB is recommended, otherwise if make doesn't succeed, try Swap.

If you run in problems withe the ```make``` command, the module "ninja" is a workaround:
 1. Make sure you have installed "ninja"
 2. Build NASTjA
    - ```cd nastja```
    - ```mkdir build```
    - ```cd build```
    - ```cmake .. -G "Ninja"```
    - ```ninja -j 1```

