# Introduction
[[_TOC_]]

## NASTjA
First at all, what is NASTjA?\
NASTjA is an acronym for *Neoteric Autonomous Stencil code for Jolly Algorithms*\
That means, NAStJA is a framework providing an easy way to enable massively parallel simulations for a wide range of multi-physics applications based on stencil algorithms.
It supports the development of parallel strategies for high-performance computing.
Modern C++ and the usage of template metaprogramming achieve excellent performance without losing ﬂexibility and usability.

One example of such an multi-physics application is the simulation of bilogical cells. 

< BILD >

But there's a variety of possible applications, other ones are Phase Field Crystal Simulations or StaRMAP (Staggered grid Radiation Moment Approximation), 
a simple method to solve spherical harmonics moment systems, such as the time-dependent PN equations, of radiative transfer.

< BILD > <BILD >

%%%%%%%%%%%%%%%%
some nice introductionary stuff here
%%%%%%%%%%%%%%%%

So let's get started. Next Topic: [The Basics](<link>)