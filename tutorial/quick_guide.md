# Quick Guide

This is a quick and short introduction into NASTjA. It's not recommended for beginners!\
Nevertheless this document can be used for a quick peak into NASTjA for checking _What is NASTjA?_.

[[_TOC_]]


## Prerequisits
- CMake 3.10+
- C++14 compliant compilter:
  - Clang 6, 7, 8, 9
  - GCC 7, 8
- Message Passing Interface (MPI) library


## Quick Build
To build NAStJA, use the standard CMake procedure
```
mkdir build
cd build
cmake ..
make
```


## Run NASTjA
You can run NASTjA as follows
```
<PATH_TO_BUILD_DIR>/nastja -o <OUTPUT_FILE> -c <CONFIG_FILE>
```

You can finde example Config-Files in ``` test/data/ ```


## Config Keys
Please see the full list of Config Keys [https://nastja.gitlab.io/nastja/docs/key.html](here).\
You can use them by manipulating the ```<CONFIG_FILE>``` directly, or use the Config String Option.

### Config String Parameter
You can specifiy the ```<CONFIG_FILE>``` by using the option ```-C <CONFIG_STRING>```.\
The ```<CONFIG_STRING>``` is organized as follows: 
```
"{'CellsInSilico':{'defaultvolume':{'value': <Value>}}}"
```
for the Key ```CellsInSilico.defaultvolume.value```


## Output for Cellular Potts Model
Please use for ```Writers```
 - *CellInfo*: general Cell Infos, like center of mass, volume, surface, ...
 - *ParallelVtkImage*: for visualization with ParaView

It's also possible to use both Writers simultaneously.

